# Contributing

## Table of Contents
[TOC]

## How to Contribute

To contribute to this project you have to request access to the [Board]() by contacting to <email>.

In the board are listed the all the tasks of this project, including the tasks ready to implement in the `To Do` column, and the ideas of future features to add to the project in the `Backlog` column.

## Configure Git

### Generate an SSH Key 

1. Open a Git Bash console
2. Run the following command: 
   ```bash
   ssh-keygen
   ```
3. Add the public key to your Bitbucket profile:
   - Log into Bitbucket
   - Go to your account (top right) > Manage Account > SSH Keys > Add Key
   - Open your public key file with Notepad++
   - Copy and paste content of the file into the entry field
   - Click Add Key

### Set your name and email

Configure your name for the commits:
```bash
git config --global user.name "YOUR NAME"
```
Configure your email for the commits:
```bash
git config --global user.email "YOUR EMAIL"
```

## Getting Code

1. Clone this repository:
   ```bash
   git clone ssh://git@bitbucket.com/<project>.git
   ```
2. Install dependencies:
   ```bash
   npm install
   ```
3. Run all tests locally. For more information about tests, read [Running Tests](#running-tests).
   ```bash
   npx wdio
   ```

## Development Workflow

The development workflow used in this project is based on [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow), o, you can use GitFlow extension or do it manually.

To use GitFlow, once you have cloned the repo, you have to initialize GitFlow in your local repo. To do this, execute:
 ```bash
 git flow init
 ```

###  Branching strategy

Every new development requires a new branch even if it's a new feature or bug fixing.

#### Feature branch naming

To create a feature branch we have to comply with the following naming convention:
`feature/story_name`.

e.g. "feature/add_readme_file"
 
#### Creating a feature branch

To start a new branch, we need to create a "Feature branch":

- Without the GitFlow extensions:
```bash
git checkout develop
git checkout -b feature/story_name
```

- When using the GitFlow extension:
```bash
git flow feature start story_name
```

#### Code Style 

Coding style is fully defined in [.eslintrc](https://bitbucket.com/projects/<project>/browse/.eslintrc), and can be used as follow:
```bash
npm run lint
```

Also a ESLint plugin can be installer in the IDE to be able to see the errors marked in different colors (We suggest to use [vscode-eslint](https://github.com/microsoft/vscode-eslint)).

Still there are some guidelines to follow:

- **Remove commented code.** If that code is commented, then isn't execute, then it isn't necessary.
- **Comments should be generally avoided.** If the code would not be understood without comments, consider re-writing the code to make it self-explanatory.
- **Include the JSDoc in all classes.** Make sure that every method in the project has associated a JSDoc to it. Some benefits of using JSDOC are:

  1.  Early detection of type errors.
  2.  Better code analysis.
  3.  Improved IDE support.
  4.  Promotes dependable refactoring.
  5.  Improves code readability.
  6.  Provides useful IntelliSense while coding.
  * For more information about JSDoc please see: [https://jsdoc.app/](https://jsdoc.app/)

### Adding New Dependencies

For all dependencies (both installation and development):

- **Do not add** a dependency if the desired functionality is easily implementable.
- If adding a dependency, it should be well-maintained and trustworthy.

A barrier for introducing new installation dependencies is especially high:

- **Do not add** installation dependency unless it's critical to project success.

### Writing Tests

Jasmine's tests are located in [`test/specs/`](https://bitbucket.com/projects/<project>/browse/test/specs), and Cucumber's tests are located in [`test/cucumber/`](https://bitbucket.com/projects/<project>/browse/test/cucumber).

Every new feature must include at least a test, not just to verify it's working as expected, but also to be used as example for people using this accelerator in their projects.

For changes on existent features, the tests must be updated or replaced by new ones, also consider the possibility to add new tests if it's required. 

Finally, tests should not depend on external services.

### Running Tests

The project includes several scripts in the `package.json` file oriented to make easy the test execution. 

Some of the script defined are: 

- `npm run test`: To run all tests using Jasmine.
- `npm run test-login`: To execute login spec file using Jasmine.
- `npm run test-tables`: To execute tables spec file using Jasmine.
- `npm run test-iframe`: To execute iframe spec file using Jasmine.
- `npm run cucumber`: To execute all tests using Cucumber
- `npm run test-mobile`: To run the tests using mobile config file and Jasmine.
- `npm run test-cloud`: To run jasmine tests using BrowserStack configuration.
- `npm run test-headless`: To execute the tests in headless mode using Jasmine.

Also it's possible to execute the tests without the pre-defined scripts by using the `npx wdio` command as follow:

1. **To execute a different config** indicate the config file to use:
   ```bash
   npx wdio ./configs/cucumber.conf.js
   ```
   This will execute all the suite included in the config file.

2. **To execute a specific spec file** add `--spec` flag and the name of the spec file to execute:
   ```bash
   npx wdio --spec iframe.spec.js
   ```
   This will execute all the tests in the spec file using the default config file.

3. **To run a predefined suite** add the property `suites` on `wdio.config` file:
   ```javascript
    suites: {
      visual: [
        './test/specs/**/visual*.js',
      ],
      login: [
        './test/specs/**/login*.js',
      ],
    }},
   ```
   Then, add `--suite` flag followed by the name of the suite:
   ```bash
   npx wdio --suite login
   ```
   This will execute all the tests in the spec files included in the suite using the default config file.

4. **To execute a specific test**, in the spec containing the test, substitute the `it` by `fit` and run the tests normally. 
   ```javascript
    ...
    fit('should work', async ({server, page}) => {
      const response = await page.goto(server.EMPTY_PAGE);
      expect(response.ok).toBe(true);
    });
   ```
   When the edited spec file is executed, only the test with `fit` will be executed.

   *Remember to return the `fit` to `it` before to commit the changes.*

5. **To exclude a complete spec file** add the property `exclude` to the config file:
   ```javascript
    exclude: [
      './test/specs/**/visual*.js',
    ],
   ```
   When the tests are executed, all the spec files in the exclude will be skipped.

6. **To disable a specific test** substitute the `it` with `xit` (mnemonic rule: '*cross it*'):
   ```javascript
    ...
    // Using "xit" to skip specific test
    xit('should work', async ({server, page}) => {
      const response = await page.goto(server.EMPTY_PAGE);
      expect(response.ok).toBe(true);
    });
   ```
   When the tests in the edited spec file are executed, the one containing the `xit` will be skipped.

### Commit Messages

Commit messages should follow the Semantic Commit Messages format:

Format: `<type>(<scope>): <subject>`

1. `<type>` is one of the following:
    - `feat` - features.
    - `fix` - bug fixes.
    - `docs` - changes to documentation.
    - `test` - changes into the tests infrastructure.
    - `devops` - build-related work, e.g. CI related patches and general changes to the browser build infrastructure.
    - `chore` - everything that doesn't fall under previous categories.

2. `<scope>` is optional. This can be a module, a class, a file, etc.

3. `<subject>` a brief description of the change.
    - it's in imperative tense, this means `change` instead of `changes` or `changed`.
    - Being clear and concise is more important than being grammar police.

*Examples:*
```
feat: add chrome for testing.
fix: Fix session cookies in firefox browser.
docs(changelog.md): Update changelog style. 
```

### Versioning

This project uses [Semantic Versioning 2.0.0](https://semver.org/), this means that the project's version will have 3 numbers that will work as follow:

| 5         | 0         | 1         |
|:---------:|:---------:|:---------:|
| **Major** | **Minor** | **Patch** |

#### Major

- Increment whenever major changes are made like architectural changes.
-	Major changes have a big impact in the way that the API is used.
- When Major is increased, Minor and Patch are clear to zero.

#### Minor

- Increment whenever minor changes are made like a new feature.
-	Minor changes don't impact the way that the API is used.
- When Minor is increased, Patch is clear to zero.

#### Patch

- Increment with bug fixes.
-	Bug fixes shouldn�t impact the way the API is used.

*Example:*

Given the version `2.6.4`, after a bug fix the new version will be `2.6.5`. Then a new feature is added to the project and the new version will be `2.7.0`. Finally, a big change that affects the way the software is used comes, once is release the new version is `3.0.0`.

```
2.6.4 -> 2.6.5 (bug fix)
      -> 2.7.0 (new feature)
      -> 3.0.0 (breaking change)
```
Also, another example is included in this [guideline from NPMJS](https://docs.npmjs.com/about-semantic-versioning).

### Code reviews

It's not allowed to merge changes **directly** in `develop` or `master` branches.

All submissions, including submissions by project members, require review. We
use **BitBucket Pull Requests** for this purpose. Consult
[Bitbucket Help](https://www.atlassian.com/es/git/tutorials/making-a-pull-request) for more
information on using pull requests.

As a contributor of this project, it's expected you perform as a reviewer for others contributors' pull requests whenever is possible.

#### Prepare your Pull Request

Before to create a new Pull Request(PR), make sure that: 

- The repo doesn't have any linting issues. (See [Code Style](#code-style))
- The version is updated in `package.json`. (See [Versioning](#versioning))
- A new entry has been added in `Changelog.md` following the existent format. 
- The branch includes latest changes in `develop` branch.
- If there are any issues, they must be resolved before to create the pull request for reviewing.

Merging rules are configured in bitbucket, so, when the PR is created, the main reviewers of the project will be added automatically. It's still possible to add more reviewers or remove the automatically added, but take in consideration that the PR can't be merged into `develop` branch until at least 1 of the main reviewers approve it. 

#### Reviewing a Pull Request

If you are assigned to a PR, once you access to the PR in Bitbucket, remember to click *'Start review'* button and then add comments with your observations, in this way they are marked as `pending`. 

Also, you should run the project in your local to verify it's working as part of your review.

Once you complete your review, click the button *'Finish review'* and select one of the PR result options:

- **Approve**: If you don't have any observation or your comments are suggestions not so relevant to be included immediatelly.
- **Request changes**: If any of your comments are changes that must be applied before the PR is merged to `develop`.
- **Decline**: If there is a big and serious issue with the PR, if the feature/bug fix is not the expected, or the implementation has something that should not be merged in `develop` and the changes required are more like a full refactor of the PR content.

## Code of Conduct

As part of <CompanyName>, all contributors must align to [<CompanyName> Code of Conduct](). 

Also there are some point specifically for this project:

- If you are assigned to a project, that project is your priority.
- This project it's a **learning space**, so be patient with those who are still learning, offer your guidance and practice your leadership too.


## Future Steps

These are some of the future enhancement that we would like to add to this project, but any new idea is also welcome.

- [WinAppDriver](https://github.com/microsoft/WinAppDriver): Windows Application Driver (WinAppDriver) is a service to support Selenium-like UI Test Automation on Windows Applications.
- [ChromeDevTools](https://developers.google.com/web/tools/chrome-devtools): The main idea is use this to be able of test messaging functionalities like chats.
- DB Connections: At this point, all the times we used this framework for clients we never needed a DB connection, but this will add a lot of value to this project.
- Jira or Azure connectors: The idea is use the API exposed by Jira or Azure(TFS) fo automatically create the test cases in those platforms based on the feature files of Cucumber writen in Gherkin.

More information about new features and bug fixing is available in the [Board]().
